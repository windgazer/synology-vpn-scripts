#!/bin/sh
# Determine the directory the script is located in
ScriptDir=$(dirname $(readlink -f $0))

if ! echo `ifconfig tun0` | grep -q "00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00"
then
    # Stop the applications
    $ScriptDir/connection-pre-down.sh

    # Start the VPN-connection again
    client_id=$(ls /usr/syno/etc/synovpnclient/openvpn/client_o* | awk -F"/" '{print $NF}' | awk -F"_" '{print $NF}')
    connection_name=$(cat /usr/syno/etc/synovpnclient/openvpn/ovpnclient.conf | grep conf_name | awk -F"=" '{print $NF}')
    echo conf_id=$client_id > /usr/syno/etc/synovpnclient/vpnc_connecting
    echo conf_name=$connection_name >> /usr/syno/etc/synovpnclient/vpnc_connecting
    echo proto=openvpn >> /usr/syno/etc/synovpnclient/vpnc_connecting

    synovpnc reconnect --protocol=openvpn --name=$connection_name

    PushBulletBody="(Re)starting '$connection_name'"
    SleepTimer=35

    # Send message to PushBullet
    nohup $ScriptDir/send_pushbullet.sh $PushBulletBody $SleepTimer &
    disown -h
else
    echo "VPN is up, no action needed."
fi
exit 0