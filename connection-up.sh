#!/bin/sh
# Determine the directory the script is located in
ScriptDir=$(dirname $(readlink -f $0))

echo "["$(date +'%Y-%m-%d %H:%M:%S')"] - Running up script"

# Call the original route-up script first
/usr/syno/etc.defaults/synovpnclient/scripts/route-up

echo "["$(date +'%Y-%m-%d %H:%M:%S')"] - Starting applications"

# Make an array of the applications we want processed
mapfile -t apps < $ScriptDir/apps.conf

# Start applications in order entered in array
for i in ${apps[@]}; do
    /usr/syno/bin/synopkg start ${i}
done

echo "["$(date +'%Y-%m-%d %H:%M:%S')"] - Starting Docker containers"
# Make an array of Docker-containers we want to process
mapfile -t containers < $ScriptDir/containers.conf

# Start the containers
docker start ${containers[@]}

# touch $ScriptDir/openvpn_conn_up
echo "["$(date +'%Y-%m-%d %H:%M:%S')"]" >> $ScriptDir/openvpn_conn_up

# Join the values to pass to the Send-Script
# function join_by { local d=$1; shift; echo -n "$1"; shift; printf "%s" "${@/#/$d}"; }
function join_by { local IFS="$1"; shift; echo "$*"; unset IFS; }

APPLIST=$(join_by , "${apps[@]}")
CONTAINERLIST=$(join_by , "${containers[@]}")

# Send message to PushBullet
nohup $ScriptDir/send_pushbullet.sh -s "UP" -t 15 -a "$APPLIST" -c "$CONTAINERLIST" &
disown -h
